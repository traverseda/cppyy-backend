diff --git a/src/core/metacling/src/TCling.cxx b/core/metacling/src/TCling.cxx
index 5413aef378..cc94115b85 100644
--- a/src/core/metacling/src/TCling.cxx
+++ b/src/core/metacling/src/TCling.cxx
@@ -1088,6 +1088,12 @@ static std::string GetModuleNameAsString(clang::Module *M, const clang::Preproce
    return std::string(llvm::sys::path::stem(ModuleName));
 }
 
+static bool FileExists(const char *file)
+{
+   struct stat buf;
+   return (stat(file, &buf) == 0);
+}
+
 static void RegisterCxxModules(cling::Interpreter &clingInterp)
 {
    if (!clingInterp.getCI()->getLangOpts().Modules)
@@ -1243,12 +1249,14 @@ TCling::TCling(const char *name, const char *title, const char* const argv[])
       // same functionality).
       if (!fCxxModulesEnabled) {
          std::string pchFilename = interpInclude + "/allDict.cxx.pch";
-         if (gSystem->Getenv("ROOT_PCH")) {
-            pchFilename = gSystem->Getenv("ROOT_PCH");
+         if (gSystem->Getenv("CLING_STANDARD_PCH")) {
+            pchFilename = gSystem->Getenv("CLING_STANDARD_PCH");
          }
 
-         clingArgsStorage.push_back("-include-pch");
-         clingArgsStorage.push_back(pchFilename);
+         if (FileExists(pchFilename.c_str())) {
+            clingArgsStorage.push_back("-include-pch");
+            clingArgsStorage.push_back(pchFilename);
+         }
       }
 
       clingArgsStorage.push_back("-Wno-undefined-inline");
diff --git a/src/core/dictgen/src/rootcling_impl.cxx b/src/core/dictgen/src/rootcling_impl.cxx
index b53b7352f9..0672f3082c 100644
--- a/src/core/dictgen/src/rootcling_impl.cxx
+++ b/src/core/dictgen/src/rootcling_impl.cxx
@@ -90,6 +90,7 @@ const char *rootClingHelp =
 #include "llvm/Support/Path.h"
 #include "llvm/Support/PrettyStackTrace.h"
 #include "llvm/Support/Signals.h"
+#include "llvm/Support/Process.h"
 
 #include "RtypesCore.h"
 #include "TModuleGenerator.h"
@@ -3840,6 +3840,10 @@ static llvm::cl::opt<bool>
 gOptNoIncludePaths("noIncludePaths",
                   llvm::cl::desc("Do not store include paths but rely on the env variable ROOT_INCLUDE_PATH."),
                   llvm::cl::cat(gRootclingOptions));
+static llvm::cl::list<std::string>
+gCxxFlags("cxxflags", llvm::cl::ZeroOrMore,
+          llvm::cl::desc("Extra C++ flags for the compiler."),
+          llvm::cl::cat(gRootclingOptions));
 static llvm::cl::opt<std::string>
 gOptISysRoot("isysroot", llvm::cl::Prefix, llvm::cl::Hidden,
             llvm::cl::desc("Specify an isysroot."),
@@ -4456,6 +4471,17 @@ int RootClingMain(int argc,
    ROOT::TMetaUtils::TClingLookupHelper helper(interp, normCtxt, 0, 0, nullptr);
    TClassEdit::Init(&helper);
 
+   // Process externally passed arguments if present.
+   llvm::Optional<std::string> EnvOpt = llvm::sys::Process::GetEnv("EXTRA_CLING_ARGS");
+   if (EnvOpt.hasValue()) {
+      StringRef Env(*EnvOpt);
+      while (!Env.empty()) {
+         StringRef Arg;
+         std::tie(Arg, Env) = Env.split(' ');
+         clingArgs.push_back(Arg.str());
+      }
+   }
+
    // flags used only for the pragma parser:
    clingArgs.push_back("-D__CINT__");
    clingArgs.push_back("-D__MAKECINT__");
@@ -5248,6 +5274,7 @@ namespace genreflex {
                        const std::vector<std::string> &warnings,
                        const std::string &rootmapFileName,
                        const std::string &rootmapLibName,
+                       const std::string &extraFlags,
                        bool interpreteronly,
                        bool doSplit,
                        bool isCxxmodule,
@@ -5309,6 +5336,12 @@ namespace genreflex {
          argvVector.push_back(string2charptr(newRootmapLibName));
       }
 
+      // any extra C++ flags picked up
+      if (!extraFlags.empty()) {
+         argvVector.push_back(string2charptr("-cxxflags"));
+         argvVector.push_back(string2charptr(extraFlags));
+      }
+
       // Interpreter only dictionaries
       if (interpreteronly)
          argvVector.push_back(string2charptr("-interpreteronly"));
@@ -5331,7 +5364,7 @@ namespace genreflex {
       AddToArgVectorSplit(argvVector, pcmsNames, "-m");
 
       // Inline the input header
-      argvVector.push_back(string2charptr("-inlineInputHeader"));
+      // argvVector.push_back(string2charptr("-inlineInputHeader"));
 
       // Write empty root pcms
       if (writeEmptyRootPCM)
@@ -5397,6 +5430,7 @@ namespace genreflex {
                            const std::vector<std::string> &warnings,
                            const std::string &rootmapFileName,
                            const std::string &rootmapLibName,
+                           const std::string &extraFlags,
                            bool interpreteronly,
                            bool doSplit,
                            bool isCxxmodule,
@@ -5433,6 +5467,7 @@ namespace genreflex {
                                           warnings,
                                           rootmapFileName,
                                           rootmapLibName,
+                                          extraFlags,
                                           interpreteronly,
                                           doSplit,
                                           isCxxmodule,
@@ -5548,6 +5583,7 @@ int GenReflexMain(int argc, char **argv)
                        SELECTIONFILENAME,
                        ROOTMAP,
                        ROOTMAPLIB,
+                       EXTRAFLAGS,
                        PCMFILENAME,
                        DEEP,
                        DEBUG,
@@ -5669,6 +5703,9 @@ int GenReflexMain(int argc, char **argv)
    const char *rootmapLibUsage =
       "--rootmap-lib\tLibrary name for the rootmap file.\n";
 
+   const char *cxxflagsUsage =
+      "--cxxflags\textra C++ compiler option flags.\n";
+
    // The Descriptor
    const ROOT::option::Descriptor genreflexUsageDescriptor[] = {
       {
@@ -5729,6 +5768,14 @@ int GenReflexMain(int argc, char **argv)
          rootmapLibUsage
       },
 
+      {
+         EXTRAFLAGS,
+         STRING ,
+         "", "cxxflags",
+         ROOT::option::FullArg::Required,
+         cxxflagsUsage
+      }, 
+
       {
          INTERPRETERONLY,
          NOTYPE,
@@ -5989,6 +6036,11 @@ int GenReflexMain(int argc, char **argv)

    bool isCxxmodule = options[CXXMODULE];

+   // Optional extra flags
+   std::string extraFlags;
+   if (options[EXTRAFLAGS])
+       extraFlags = options[EXTRAFLAGS].arg;
+
    bool multidict = false;
    if (options[MULTIDICT]) multidict = true;
 
@@ -6075,6 +6127,7 @@ int GenReflexMain(int argc, char **argv)
                                     warnings,
                                     rootmapFileName,
                                     rootmapLibName,
+                                    extraFlags,
                                     interpreteronly,
                                     doSplit,
                                     isCxxmodule,
@@ -6097,6 +6150,7 @@ int GenReflexMain(int argc, char **argv)
                                         warnings,
                                         rootmapFileName,
                                         rootmapLibName,
+                                        extraFlags,
                                         interpreteronly,
                                         doSplit,
                                         isCxxmodule,
diff --git a/src/etc/dictpch/makepch.py b/src/etc/dictpch/makepch.py
index dd8faabc13..f633342f5c 100755
--- a/src/etc/dictpch/makepch.py
+++ b/src/etc/dictpch/makepch.py
@@ -41,6 +41,12 @@ def getCppFlags(cppflagsFilename):
    ifile = open(cppflagsFilename)
    lines = ifile.readlines()
    ifile.close()
+   if "std" in os.environ.get("EXTRA_CLING_ARGS", ""):
+      keep = []
+      for line in lines:
+         if not "std" in line:
+             keep.append(line)
+      lines = keep
    cppFlags = " ".join(map(lambda line: line[:-1], lines))
    return cppFlags
 
@@ -79,8 +79,12 @@
       rootbuildFlag="-rootbuild"


-   cppFlags = getCppFlags(cppflagsFilename)
+   cppFlags = getCppFlags(cppflagsFilename).split()

+   if "-isystem" in cppFlags:
+      idx = cppFlags.index("-isystem")
+      del cppFlags[idx: idx+2]
+
    cppflagsList=["-D__CLING__",
                  "-D__STDC_LIMIT_MACROS",
                  "-D__STDC_CONSTANT_MACROS",
@@ -89,7 +93,7 @@
                  "-I%s" %os.path.join(rootdir,"etc"),
                  "-I%s" %os.path.join(rootdir,cfgdir),
                  "-I%s" %os.path.join(rootdir,"etc","cling"),
-                 cppFlags]
+                ] + cppFlags

    cppflagsList.append(extraCppflags)

diff --git a/src/config/RConfigure.in b/src/config/RConfigure.in
index e8210e2f65..63b2a8dc99 100644
--- a/src/config/RConfigure.in
+++ b/src/config/RConfigure.in
@@ -65,4 +65,64 @@
 #@hastmvacudnn@ R__HAS_CUDNN /**/

 
+#if __cplusplus > 201402L
+#ifndef R__USE_CXX17
+#define R__USE_CXX17
+#endif
+#ifdef R__USE_CXX14
+#undef R__USE_CXX14
+#endif
+#ifdef R__USE_CXX11
+#undef R__USE_CXX11
+#endif
+
+#ifndef R__HAS_STD_STRING_VIEW
+#define R__HAS_STD_STRING_VIEW
+#endif
+#ifdef R__HAS_STD_EXPERIMENTAL_STRING_VIEW
+#undef R__HAS_STD_EXPERIMENTAL_STRING_VIEW
+#endif
+#ifdef R__HAS_STOD_STRING_VIEW
+#undef R__HAS_STOD_STRING_VIEW
+#endif
+
+#ifndef R__HAS_STD_INVOKE
+#define R__HAS_STD_INVOKE
+#endif
+#ifndef R__HAS_STD_APPLY
+#define R__HAS_STD_APPLY
+#endif
+
+#ifndef R__HAS_STD_INDEX_SEQUENCE
+#define R__HAS_STD_INDEX_SEQUENCE
+#endif
+
+#elif __cplusplus > 201103L
+#ifdef R__USE_CXX17
+#undef R__USE_CXX17
+#endif
+#ifndef R__USE_CXX14
+#define R__USE_CXX14
+#endif
+#ifdef R__USE_CXX11
+#undef R__USE_CXX11
+#endif
+
+#ifndef R__HAS_STD_INDEX_SEQUENCE
+#define R__HAS_STD_INDEX_SEQUENCE
+#endif
+
+#else
+#ifdef R__USE_CXX17
+#undef R__USE_CXX17
+#endif
+#ifdef R__USE_CXX14
+#undef R__USE_CXX14
+#endif
+#ifndef R__USE_CXX11
+#define R__USE_CXX11
+#endif
+
+#endif
+
 #endif
diff --git a/src/build/unix/makepchinput.py b/src/build/unix/makepchinput.py
index 1468a87277..c823c5eef7 100755
--- a/src/build/unix/makepchinput.py
+++ b/src/build/unix/makepchinput.py
@@ -53,9 +53,10 @@ def getGuardedStlInclude(headerName):
 #-------------------------------------------------------------------------------
 def getSTLIncludes():
    """
-   Here we include the list of c++11 stl headers
+   Here we include the list of c++11/14/17 stl headers
    From http://en.cppreference.com/w/cpp/header
    valarray is removed because it causes lots of compilation at startup.
+   codecvt, ctgmath, and cstdbool are removed as they are deprecated in C++17
    """
    stlHeadersList = ("cstdlib",
                      "csignal",
@@ -123,7 +124,7 @@ def getSTLIncludes():
                      "cstdio",
                      "locale",
                      "clocale",
-                     "codecvt",
+#                    "codecvt",
                      "atomic",
                      "thread",
                      "mutex",
@@ -131,9 +132,21 @@ def getSTLIncludes():
                      "condition_variable",
                      "ciso646",
                      "ccomplex",
-                     "ctgmath",
+#                    "ctgmath",
                      "regex",
-                     "cstdbool")
+#                    "cstdbool",
+                     # add C++14 headers
+                     "shared_mutex",
+                     # add C++17 headers
+                     "any",
+                     "optional",
+                     "variant",
+                     "memory_resource",
+                     "string_view",
+                     "charconv",
+#                    "execution",
+                     "filesystem",
+                     )

    allHeadersPartContent = "// STL headers\n"

diff --git a/src/core/CMakeLists.txt b/src/core/CMakeLists.txt
index f348274986..a21ae827a4 100644
--- a/src/core/CMakeLists.txt
+++ b/src/core/CMakeLists.txt
@@ -161,7 +161,6 @@ ROOT_GENERATE_DICTIONARY(G__Core
   MODULE
     Core
   OPTIONS
-    -writeEmptyRootPCM
   LINKDEF
     base/inc/LinkDef.h
 )
diff --git a/src/interpreter/cling/lib/Interpreter/CIFactory.cpp b/src/interpreter/cling/lib/Interpreter/CIFactory.cpp
index 80d1b1c52a..58edb8c26d 100644
--- a/src/interpreter/cling/lib/Interpreter/CIFactory.cpp
+++ b/src/interpreter/cling/lib/Interpreter/CIFactory.cpp
@@ -52,6 +52,8 @@
 #include <ctime>
 #include <memory>
 
+#include <sys/stat.h>
+
 using namespace clang;
 using namespace cling;
 
@@ -337,7 +339,27 @@ namespace {
       }
 
   #ifdef CLING_OSX_SYSROOT
-    sArguments.addArgument("-isysroot", CLING_OSX_SYSROOT);
+      {
+        std::string sysroot{CLING_OSX_SYSROOT};
+        struct stat buf;
+        if (stat(sysroot.c_str(), &buf) == 0) {
+          sArguments.addArgument("-isysroot", CLING_OSX_SYSROOT);
+        } else {
+        // attempt to find a proper sysroot, or leave it up to Clang and user
+        // defined variables such as SDKROOT and MACOSX_DEPLOYMENT_TARGET
+          std::string::size_type pos = sysroot.rfind("SDKs/MacOSX");
+          if (pos != std::string::npos) {
+            if (getenv("MACOSX_DEPLOYMENT_TARGET"))
+              sysroot = sysroot.substr(0, pos+11) + getenv("MACOSX_DEPLOYMENT_TARGET") + ".sdk";
+            else
+              sysroot = sysroot.substr(0, pos+11)+".sdk";  // generic location
+            if (stat(sysroot.c_str(), &buf) == 0)
+              sArguments.addArgument("-isysroot", sysroot.c_str());
+            else
+              cling::errs() << "Warning: sysroot \"" << sysroot << "\" not found (ignoring for now).";
+          }
+        }
+      }
   #endif
 
 #endif // _MSC_VER
diff --git a/src/core/clingutils/CMakeLists.txt b/src/core/clingutils/CMakeLists.txt
index 4be71a05fb..b126c3d9b4 100644
--- a/src/core/clingutils/CMakeLists.txt
+++ b/src/core/clingutils/CMakeLists.txt
@@ -41,31 +41,31 @@ ROOT_INSTALL_HEADERS()
 
 #### STL dictionary (replacement for cintdlls)##############################
 
-set(stldicts
-    vector
-    list
-    forward_list
-    deque
-    map map2 unordered_map
-    multimap multimap2 unordered_multimap
-    set unordered_set
-    multiset unordered_multiset
-    complex)
-if(NOT WIN32)
-  list(APPEND stldicts valarray)
-endif()
-include_directories(${CMAKE_BINARY_DIR}/etc/cling/cint)
-foreach(dict ${stldicts})
-  string(REPLACE "2" "" header ${dict})
-  string(REPLACE "complex" "root_std_complex.h" header ${header})
-  string(REPLACE "multi" "" header ${header})
-  ROOT_STANDARD_LIBRARY_PACKAGE(${dict}Dict
-                                NO_SOURCES NO_INSTALL_HEADERS NO_CXXMODULE
-                                STAGE1
-                                NODEPHEADERS ${header}
-                                LINKDEF src/${dict}Linkdef.h
-                                DEPENDENCIES Core)
-endforeach()
+#set(stldicts
+#    vector
+#    list
+#    forward_list
+#    deque
+#    map map2 unordered_map
+#    multimap multimap2 unordered_multimap
+#    set unordered_set
+#    multiset unordered_multiset
+#    complex)
+#if(NOT WIN32)
+#  list(APPEND stldicts valarray)
+#endif()
+#include_directories(${CMAKE_BINARY_DIR}/etc/cling/cint)
+#foreach(dict ${stldicts})
+#  string(REPLACE "2" "" header ${dict})
+#  string(REPLACE "complex" "root_std_complex.h" header ${header})
+#  string(REPLACE "multi" "" header ${header})
+#  ROOT_STANDARD_LIBRARY_PACKAGE(${dict}Dict
+#                                NO_SOURCES NO_INSTALL_HEADERS NO_CXXMODULE
+#                                STAGE1
+#                                NODEPHEADERS ${header}
+#                                LINKDEF src/${dict}Linkdef.h
+#                                DEPENDENCIES Core)
+#endforeach()
 
 set(CLANG_RESOURCE_DIR_STEM)
 if (builtin_clang)
diff --git a/src/main/CMakeLists.txt b/src/main/CMakeLists.txt
index 32094d55bd..30d55635b1 100644
--- a/src/main/CMakeLists.txt
+++ b/src/main/CMakeLists.txt
@@ -85,7 +85,7 @@ ROOT_EXECUTABLE(rootcling src/rootcling.cxx
 # rootcling includes the ROOT complex header which would build the complex
 # dictionary with modules. To make sure that rootcling_stage1 builds this
 # dict before we use it, we add a dependency here.
-add_dependencies(rootcling complexDict)
+#add_dependencies(rootcling complexDict)
 
 target_include_directories(rootcling PRIVATE
         ${CMAKE_CURRENT_SOURCE_DIR}/../core/metacling/res
diff --git a/src/interpreter/cling/lib/Interpreter/IncrementalParser.cpp b/src/interpreter/cling/lib/Interpreter/IncrementalParser.cpp
index 2593c21fb2..dbe8caa04b 100644
--- a/src/interpreter/cling/lib/Interpreter/IncrementalParser.cpp
+++ b/src/interpreter/cling/lib/Interpreter/IncrementalParser.cpp
@@ -91,20 +91,7 @@ namespace {
       "  Failed to extract C++ standard library version.\n";
     }
 
-    if (CLING_CXXABI_BACKWARDCOMP && CurABI < CLING_CXXABI_VERS) {
-       // Backward compatible ABIs allow us to interpret old headers
-       // against a newer stdlib.so.
-       return true;
-    }
-
-    cling::errs() <<
-      "Warning in cling::IncrementalParser::CheckABICompatibility():\n"
-      "  Possible C++ standard library mismatch, compiled with "
-      << CLING_CXXABI_NAME << " '" << CLING_CXXABI_VERS << "'\n"
-      "  Extraction of runtime standard library version was: '"
-      << CurABI << "'\n";
-
-    return false;
+    return true;
   }
 
   class FilteringDiagConsumer : public cling::utils::DiagnosticsOverride {
diff --git a/src/core/metacling/src/TCling.cxx b/src/core/metacling/src/TCling.cxx
index 5413aef378..e611986dbf 100644
--- a/src/core/metacling/src/TCling.cxx
+++ b/src/core/metacling/src/TCling.cxx
@@ -1844,26 +1884,7 @@ namespace {
 static const std::unordered_set<std::string> gIgnoredPCMNames = {"libCore",
                                                                  "libRint",
                                                                  "libThread",
-                                                                 "libRIO",
-                                                                 "libImt",
-                                                                 "libcomplexDict",
-                                                                 "libdequeDict",
-                                                                 "liblistDict",
-                                                                 "libforward_listDict",
-                                                                 "libvectorDict",
-                                                                 "libmapDict",
-                                                                 "libmultimap2Dict",
-                                                                 "libmap2Dict",
-                                                                 "libmultimapDict",
-                                                                 "libsetDict",
-                                                                 "libmultisetDict",
-                                                                 "libunordered_setDict",
-                                                                 "libunordered_multisetDict",
-                                                                 "libunordered_mapDict",
-                                                                 "libunordered_multimapDict",
-                                                                 "libvalarrayDict",
-                                                                 "G__GenVector32",
-                                                                 "G__Smatrix32"};
+                                                                 "libRIO"};

 static void PrintDlError(const char *dyLibName, const char *modulename)
 {
diff --git a/src/core/cont/inc/LinkDef.h b/src/core/cont/inc/LinkDef.h
index 2fdcd76639..39b362cca4 100644
--- a/src/core/cont/inc/LinkDef.h
+++ b/src/core/cont/inc/LinkDef.h
@@ -55,11 +55,6 @@
 #pragma link C++ class TRefTable-;
 #pragma link C++ class TVirtualCollectionProxy-;

-#pragma link C++ class std::vector<Int_t>;
-#pragma link C++ class std::vector<Int_t>::iterator;
-#pragma link C++ class std::vector<Int_t>::const_iterator;
-#pragma link C++ class std::vector<Int_t>::reverse_iterator;
-
 #pragma link C++ nestedclass;
 #pragma link C++ nestedtypedef;

diff --git a/src/core/base/inc/LinkDef2.h b/src/core/base/inc/LinkDef2.h
index 44883b3c03..8f4d6c64a6 100644
--- a/src/core/base/inc/LinkDef2.h
+++ b/src/core/base/inc/LinkDef2.h
@@ -10,34 +10,11 @@
 
 #ifdef __CINT__
 
-#ifdef __CLING__
-#include <string>
-#pragma link C++ class string::iterator;
-#pragma link C++ class string::const_iterator;
-#else
-#include "dll_stl/str.h"
-#endif
+#pragma link C++ global gTQSender;
+#pragma link C++ global gTQSlotParams;
 
-#pragma extra_include "vector";
 #pragma extra_include "string";
-
 #pragma create TClass string;
-#pragma link C++ class std::vector<string>;
-#pragma link C++ operator std::vector<string>;
-#pragma link C++ class std::vector<string>::iterator;
-#pragma link C++ class std::vector<string>::const_iterator;
-#pragma link C++ class std::vector<string>::reverse_iterator;
-
-#pragma link C++ class std::vector<TString>;
-#pragma link C++ operators std::vector<TString>;
-#pragma link C++ class std::vector<TString>::iterator;
-#pragma link C++ class std::vector<TString>::const_iterator;
-#pragma link C++ class std::vector<TString>::reverse_iterator;
-
-#include <vector>
-
-#pragma link C++ global gTQSender;
-#pragma link C++ global gTQSlotParams;
 
 #pragma link C++ enum EAccessMode;
 #pragma link C++ enum ESignals;

diff --git a/src/core/meta/inc/LinkDef.h b/src/core/meta/inc/LinkDef.h
index e36a6a09f1..17101803f4 100644
--- a/src/core/meta/inc/LinkDef.h
+++ b/src/core/meta/inc/LinkDef.h
@@ -62,7 +62,6 @@
 #pragma link C++ class TStreamerElement-;
 #pragma link C++ class TToggle;
 #pragma link C++ class TToggleGroup;
-#pragma link C++ class std::vector<std::pair<Int_t, Int_t> >+;
 #pragma link C++ class TFileMergeInfo;
 #pragma link C++ class TListOfFunctions+;
 #pragma link C++ class TListOfFunctionsIter;
@@ -72,8 +71,5 @@
 #pragma link C++ class TListOfEnumsWithLock+;
 #pragma link C++ class TListOfEnumsWithLockIter;
 #pragma link C++ class ROOT::Detail::TStatusBitsChecker-;
-//for new protoclasses
-#pragma link C++ class std::vector<TDataMember * >+;
-#pragma link C++ class std::vector<TProtoClass::TProtoRealData >+;

 #endif
diff --git a/src/math/mathcore/inc/LinkDef2.h b/src/math/mathcore/inc/LinkDef2.h
index c3e163b919..3259ba9488 100644
--- a/src/math/mathcore/inc/LinkDef2.h
+++ b/src/math/mathcore/inc/LinkDef2.h
@@ -22,10 +22,6 @@
 //#pragma link C++ class ROOT::Math;
 #endif

-#pragma link C++ class std::vector<Double_t>::iterator;
-#pragma link C++ class std::vector<Double_t>::const_iterator;
-#pragma link C++ class std::vector<Double_t>::reverse_iterator;
-
 #pragma link C++ global gRandom;

 #pragma link C++ class TRandom+;
@@ -174,7 +170,6 @@
 #pragma link C++ class ROOT::Math::DistSampler+;
 #pragma link C++ class ROOT::Math::DistSamplerOptions+;
 #pragma link C++ class ROOT::Math::GoFTest+;
-#pragma link C++ class std::vector<std::vector<double> >+;

 #pragma link C++ class ROOT::Math::Delaunay2D+;

diff --git a/src/math/mathcore/inc/LinkDef3.h b/src/math/mathcore/inc/LinkDef3.h
index 16006bc20f..f89fa58b90 100644
--- a/src/math/mathcore/inc/LinkDef3.h
+++ b/src/math/mathcore/inc/LinkDef3.h
@@ -55,6 +55,4 @@
 #pragma link C++ function ROOT::Fit::Fitter::LikelihoodFit(const ROOT::Fit::BinData &, bool);
 #pragma link C++ function ROOT::Fit::Fitter::LikelihoodFit(const ROOT::Fit::UnBinData &, bool);

-#pragma link C++ class std::vector<ROOT::Fit::ParameterSettings>;
-
 #endif
